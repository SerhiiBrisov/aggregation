using System;

namespace Aggregation
{
    public class SpecialDeposit : Deposit
    {
        public SpecialDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {

        }
        public override decimal Income()
        {
            decimal income = Amount;
            for (int i = 1; i <= this.Period; i++)
            {
                income += income * i / 100;
            }
            return decimal.Round((income - Amount), 2, MidpointRounding.AwayFromZero);

        }
    }
}
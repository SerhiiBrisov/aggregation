using System;

namespace Aggregation
{
    public class LongDeposit : Deposit
    {
        private const int minTerm = 6;
        private const int longRate = 15;

        public LongDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {

        }
        public override decimal Income()
        {
            decimal income = Amount;
            for (int i = 1; i <= this.Period; i++)
            {
                if (i> minTerm)
                {
                    income += income * longRate / 100;
                }
            }
            return decimal.Round((income - Amount), 2, MidpointRounding.AwayFromZero);
        }
    }
}